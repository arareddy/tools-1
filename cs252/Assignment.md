# Assingments for CS252: July-Dec 2016.


## Assignment 1. (Aug 6, 2016)

All these assignments have to be demonstrated in front of the TA. You
will be asked questions on it.

1. Show the network parameters like IP address, netmask, default
   gateway etc of your cloud container. You need to show these
   parameters on the shell command line using appropriate shell
   commands. It is *not enough* to show it on you admin panel.

2. Find out all networking services are running on your system that
   uses some tcp or udp port.

3. Find the network route, i.e. the intermediate nodes in the network,
   that is taken by a packet sent from here to www.iitb.ac.in.

4. Block access to ssh on your machine form vyom.cc.iitk.ac.in


# Assignment 2. (Aug 16, 2016)

1. Create a internal lan inside the cloud with the following configuration

   * LAN A contains machine X and Y
   * LAN B contains machine Z
   * Select two distinct address space of the LANS.
   * Connect the two LANs with a router.

2. After logging in to X (via the web console), get the hardware address of Y and the router.

3. Find out what arping does. From the machine X, which of Y and Z
   will be accessible via `arping` and why ?

# Assignment 3. (Aug 23, 2016).

NOTE: Packets in the internal network gets dropped if its destiantion
does not fit into the internal lans subnet. So we are simplifying the
assignment.

Start by setting up a machine (let us call it `A`) which connects to our
cse network. You need to write iptable rules to do the following.

1. Ping from `A` to `vyom@cc` should not work where as ping from A to
   `turing@cse` should work.

2. Ping _from_ `vyom@cc` _to_ `A` should not work where as ping from
   `turing@cse` should work.

2. An ssh connection from vyom to A should be dropped where as ssh
   connection from `turing@cse` should function normally.

Note that all these should be achieved through IPtable rules and _not_
by other means.
